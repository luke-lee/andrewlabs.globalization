﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleAppDemo
{
    class Program
    {

        // DEMO 測試案例:
        // 1. 設定 InfraContext 為 Taiwan, 9487 號店, 按照 config 及 infra 設定顯示當地的日期時間與金錢資訊。包含明確指定台幣與馬幣的資訊。
        //    所有的設定都直接參考系統內建的 cultureinfo, 若有不同需求則可透過 config override 系統預設設定值。
        // 2. 設定 InfraContext 為 Malaysia, 9487 號店, 其餘同上
        // 3. 同 (2), 但是把特定 9487 號店的預設語系改為 en-US, 其餘同上
        // 4. 同 (3), 該店要調整 currency symbol 的顯示方式

        static void Main(string[] args)
        {
            Init();
            Run();
        }

        static void Init()
        {
            //
            // 從外界環境 (ex: URL / browser / VM config) 資訊，決定服務的 Infra Context 資訊。InfraContext 包含:
            //  1. 服務範圍 市場 (tw, Taiwan)
            //  2. (option) 服務客戶ID (shopid)
            //  3. (option) 服務所在地區 (data center)
            //


            InfraContext.Current =
            //new InfraContext()
            //{
            //    Id = "tw",
            //    Market = "Taiwan",
            //    ShopId = 9527,
            //    DataCenter = "ap-northeast-1"
            //};
            new InfraContext()
            {
                Id = "my",
                Market = "Malaysia",
                ShopId = 9487,
                DataCenter = "ap-northeast-1"
            };


            // set culture info
            CultureInfo culture = ConfigHelper.GetCulture();
            CultureInfo.CurrentCulture = culture;
            CultureInfo.CurrentUICulture = culture;
        }

        static void Run()
        {
            //
            //  MSDN: 開發世界性的應用程式的最佳作法
            //  https://docs.microsoft.com/zh-tw/dotnet/standard/globalization-localization/best-practices-for-developing-world-ready-apps
            //

            // infra environment
            Console.WriteLine("Infra Environment:");
            Console.WriteLine($"- ID:         {InfraContext.Current.Id}");
            Console.WriteLine($"- Market:     {InfraContext.Current.Market}");
            Console.WriteLine($"- ShopId:     {InfraContext.Current.ShopId}");
            Console.WriteLine($"- DataCenter: {InfraContext.Current.DataCenter}");


            // https://docs.microsoft.com/zh-tw/dotnet/standard/base-types/formatting-types

            // display
            Console.WriteLine();
            Console.WriteLine("Display Format Demo:");
            Console.WriteLine("- Short Date:                   {0:d}", DateTime.Now);
            Console.WriteLine("- Long Date:                    {0:D}", DateTime.Now);
            Console.WriteLine("- Short Time:                   {0:t}", DateTime.Now);
            Console.WriteLine("- Long Time:                    {0:T}", DateTime.Now);

            Console.WriteLine("- Currency:                     {0:C}", 9527);
            Console.WriteLine("- Currency Symbol:              {0}", CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol);
            Console.WriteLine("- Currency (without symbol):    {0:N}", 9527);
            Console.WriteLine("- Currency (with HTML format):  <b>{0}</b>{1:N}", CultureInfo.CurrentCulture.NumberFormat.CurrencySymbol, 9527);
            



            // 無視目前的地區語言選項，若要配合 business logic 顯示特定的幣別資訊，請用下列寫法:
            Console.WriteLine();
            Console.WriteLine("- Taiwan Currency:              {0}", 9527.ToString("C", ConfigHelper.GetCulture("tw", InfraContext.Current.ShopId)));
            Console.WriteLine("- Malaysia Currency:            {0}", 9527.ToString("C", ConfigHelper.GetCulture("my", InfraContext.Current.ShopId)));

            // api or store
            Console.WriteLine();
            Console.WriteLine("- ISO 8601 Short Time:          {0:o}", DateTime.Now);
            Console.WriteLine("- ISO 8601 Full Time:           {0:O}", DateTime.Now);
            Console.WriteLine("- ISO 8601 (UTC) Short Time:    {0:o}", DateTime.UtcNow);
            Console.WriteLine("- ISO 8601 (UTC) Full Time:     {0:O}", DateTime.UtcNow);

            // resource
            Console.WriteLine();
            Console.WriteLine("- strDemo1:                     {0}", globalization.strDemo1);
        }
    }



    public class InfraContext
    {
        public string Id { get; set; }
        public string Market { get; set; }
        public int ShopId { get; set; }
        public string DataCenter { get; set; }

        public static InfraContext Current { get; set; }
    }

    public class ConfigHelper
    {
    
        public static string GetConfig(string path)
        {
            return GetConfig(InfraContext.Current.Id, InfraContext.Current.ShopId, path);
        }

        public static string GetConfig(InfraContext infra, string path)
        {
            return GetConfig(infra.Id, infra.ShopId, path);
        }

        public static string GetConfig(string infra, int shop, string path)
        {
            foreach (string fullpath in new string[]
            {
                $"/{infra}/{shop}/{path}",
                $"/{infra}/$shard/{path}",
                $"/$global/$shard/{path}",
            })
            {
                if (_storage.ContainsKey(fullpath)) return _storage[fullpath];
            }
            return null;
        }

        public static CultureInfo GetCulture()
        {
            return GetCulture(InfraContext.Current.Id, InfraContext.Current.ShopId);
        }
        public static CultureInfo GetCulture(string id, int shop = 0)
        {
            //new CultureInfo(ConfigHelper.GetConfig(id, shop, "internationalization/default-language"));

            // set culture info
            CultureInfo culture =
                //new CultureInfo("ms-MY");
                //new CultureInfo("zh-TW");
                //CultureInfo.InvariantCulture;
                //new CultureInfo(ConfigHelper.GetConfig("internationalization/default-language"));
                new CultureInfo(ConfigHelper.GetConfig(id, shop, "internationalization/default-language"));

            // customize display format
            string result = null;

            result = ConfigHelper.GetConfig(id, shop, "internationalization/currency-format/digits-after-decimal");
            if (string.IsNullOrEmpty(result) == false)
            {
                culture.NumberFormat.CurrencyDecimalDigits = int.Parse(result);

                // NOTE: 為了自訂 currency html, 因此統一 currency / number 的格式。
                culture.NumberFormat.NumberDecimalDigits = int.Parse(result);
            }

            result = ConfigHelper.GetConfig(id, shop, "internationalization/currency-format/symbol");
            if (string.IsNullOrEmpty(result) == false)
            {
                culture.NumberFormat.CurrencySymbol = result;
            }

            return culture;
        }


        private static Dictionary<string, string> _storage = new Dictionary<string, string>()
        {
            { "/tw/9527/internationalization/default-language", "en-US" },
            { "/tw/$shard/internationalization/default-language", "zh-TW" },
            { "/tw/$shard/internationalization/currency-format/symbol", "NT$" },
            { "/tw/$shard/internationalization/currency-format/digits-after-decimal", "0" },

            //{ "/my/9487/internationalization/default-language", "en-US" },
            { "/my/$shard/internationalization/default-language", "ms-MY" },
            //{ "/my/$shard/internationalization/currency-format/symbol", "RM _____ $" },
            //{ "/my/$shard/internationalization/currency-format/digits-after-decimal", "0" },

            { "/$global/$shard/internationalization/default-language", "en-US" },
            //{ "/$global/$shard/internationalization/currency-format/symbol", "$" },
            //{ "/$global/$shard/internationalization/currency-format/digits-after-decimal", "2" },
        };


    }
    
}
